local P1 = {
    score = 0,
    x = 0,
    y = 40;
    width = 15,
    height = 60
}

local P2 = {
    score = 0,
    x = 0,
    y = 40;
    width = 15,
    height = 60
}

local BALL = {
    x = 0,
    dx = 0,
    y = 0,
    dy = 0,
    r = 15
}

local PADDLE_SPEED = 350
local BALL_SPEED = 0.4
local UpperBound = 40

local GameState = "start"

local start_text_width = 0
local Start_MSG = "Press Space to start"

local difficulty_increase = 30

local win = 10

function love.load()
    love.graphics.setDefaultFilter("nearest", "nearest")
    WIDTH, HEIGHT = love.graphics.getDimensions()
    -- resizing makes the game play to fast
    -- love.window.setMode(WIDTH, HEIGHT, {resizable = true})

    -- sounds from http://sfxr.me/
    Sounds = {
        ['player_hit'] = love.audio.newSource('resources/sounds/player.wav', 'static'),
        ['score'] = love.audio.newSource('resources/sounds/score.wav', 'static'),
        ['wall_hit'] = love.audio.newSource('resources/sounds/wall.wav', 'static')
    }

    -- https://www.1001fonts.com/joystix-font.html
    JoystixFont = love.graphics.newFont("resources/joystix_monospace.ttf", 35)

    math.randomseed(os.time())
    start_text_width = JoystixFont:getWidth(Start_MSG)
    P1.x = 10
    P2.x = WIDTH - 10 - P2.width
    Init()
end


function love.update(dt)
    if GameState == "play" then
        -- input handling P1
        if love.keyboard.isDown('w') then
            P1.y = math.max(UpperBound,P1.y - PADDLE_SPEED * dt)
        elseif love.keyboard.isDown('s') then
            P1.y = math.min(HEIGHT - P1.height ,P1.y + PADDLE_SPEED * dt)
        end
        
        -- input handling P2
        if love.keyboard.isDown('up') then
            P2.y = math.max(UpperBound,P2.y - PADDLE_SPEED * dt)
        elseif love.keyboard.isDown('down') then
            P2.y = math.min(HEIGHT - P2.height ,P2.y + PADDLE_SPEED * dt)
        end
    
        -- move ball towards destination
        BALL.x = BALL.x + BALL.dx * dt * BALL_SPEED
        BALL.y = BALL.y + BALL.dy * dt * BALL_SPEED
        
        -- check for player 1 and ball collision
        if (BALL.x - BALL.r) < (P1.x + P1.width) and BALL.y >= P1.y and BALL.y <= (P1.y + P1.height) then
            BALL.x = BALL.x + P1.width + 1
            Sounds['player_hit']:play()
            if BALL.dx < 0 then
                BALL.dx = -BALL.dx + math.random(difficulty_increase)
            else
                BALL.dx = -BALL.dx - math.random(difficulty_increase)
            end
        end
        -- check for player 2 and ball collision
        if (BALL.x + BALL.r) > P2.x and BALL.y >= P2.y and BALL.y <= (P2.y + P2.height) then
            BALL.x = BALL.x - 1
            Sounds['player_hit']:play()
            if BALL.dx < 0 then
                BALL.dx = -BALL.dx + math.random(difficulty_increase)
            else
                BALL.dx = -BALL.dx - math.random(difficulty_increase)
            end
        end
    
        -- left wall collision
        if (BALL.x - BALL.r) <= 0 then
            Sounds['score']:play()
            BALL.x = BALL.r + 1
            BALL.dx = -BALL.dx
            P2.score = P2.score + 1
            Init()
        -- right wall collision
        elseif (BALL.x + BALL.r) >= WIDTH then
            Sounds['score']:play()
            BALL.x = BALL.x - 1
            BALL.dx = -BALL.dx
            P1.score = P1.score + 1
            Init()
        end
    
        -- upper wall collision
        if (BALL.y - BALL.r) <= UpperBound then
            Sounds['wall_hit']:play()
            BALL.y = UpperBound + BALL.r + 1
            BALL.dy = -BALL.dy
        -- bottom wall collision
        elseif (BALL.y + BALL.r) >= HEIGHT then
            Sounds['wall_hit']:play()
            BALL.y = HEIGHT - BALL.r - 1
            BALL.dy = -BALL.dy
        end
    end
end


function love.draw()
    if GameState == "start" then
        love.graphics.setColor(1, 1, 1)
        love.graphics.print(Start_MSG, (WIDTH - start_text_width) / 2, HEIGHT - 100)
    elseif GameState == "end" then
        love.graphics.setColor(1, 1, 1)
        love.graphics.print(Start_MSG, (WIDTH - start_text_width) / 2, HEIGHT - 100)

    end
    -- set font
    love.graphics.setFont(JoystixFont)
    love.graphics.setColor(1, 1, 1)
    -- draw players and score
    love.graphics.setLineWidth( 5 )
    love.graphics.line(0, UpperBound, WIDTH, 40)
    love.graphics.printf(P1.score, 0, 0, 100, "left")
    love.graphics.printf(P2.score, WIDTH-100, 0, 100, "right")
    -- love.graphics.printf(BALL.dx, WIDTH/2, 0, 200, "center")
    love.graphics.setColor(0, 0, 1)
    love.graphics.rectangle("fill", P1.x, P1.y, P1.width, P1.height)
    love.graphics.setColor(1, 0, 0)
    love.graphics.rectangle("fill", P2.x, P2.y, P2.width, P2.height)
    -- ball 
    love.graphics.setColor(153/255, 0, 139/255)
    love.graphics.circle("fill", BALL.x, BALL.y, BALL.r)
end


function love.keypressed(key)
    if key == "escape" then
        love.event.quit(0)
    elseif key == "space" then
        if GameState == "start" then
            GameState = "play"
        elseif GameState == "play" then

        elseif GameState == "end" then
            P1.score = 0
            P2.score = 0
            GameState = "start"
            UpdateText("Press space to start")
        end
    end
end

function love.resize(w , h )
    WIDTH = w
    HEIGHT = h
    P2.x = WIDTH - 10 - P2.width
end

function Init()
    GameState = "start"
    UpdateText("Press space to start")
    P1.y = HEIGHT/2
    P2.y = HEIGHT/2
    BALL.x = WIDTH/2
    BALL.y = HEIGHT/2
    if math.random(2) == 1 then
        BALL.dx =  WIDTH * 0.75
    else
        BALL.dx =  -WIDTH * 0.75
    end
    BALL.dy = math.random(UpperBound,HEIGHT)
    CheckWin()
end

function CheckWin()
    if P1.score == win or P2.score == win then
        GameState = "end"
        if P1.score == win then
            UpdateText("Player 1 won\nPress space to restart")
        elseif P2.score == win then
            UpdateText("Player 2 won\nPress space to restart")
        end
    end
end

function UpdateText(text)
    Start_MSG = text
    start_text_width = JoystixFont:getWidth(Start_MSG)
end