extends Node

# art from https://opengameart.org/content/rocket
# sound from http://sfxr.me/
# font from https://www.1001freefonts.com/request.font
# sounds win https://freesound.org/people/jivatma07/sounds/122255
# bg music https://freesound.org/people/esistnichtsoernst/sounds/473996/
# icons https://fonts.google.com/icons?selected=Material+Icons&icon.query=sound
# https://opengameart.org/content/asteroid-l


onready var scene_tree: = get_tree()
onready var pause_overlay:ColorRect = $PauseLayer/UserInterface/PauseOverlay

export (PackedScene) var asteroid_scene


func togglePause():
	var isPaused = !scene_tree.paused
	scene_tree.paused = isPaused
	pause_overlay.visible  = isPaused

func _ready() -> void:
	$Level/Platform.connect("win", self, "win")
	$PauseLayer/UserInterface.connect("rotationChange", self, "rotChange")
	$PauseLayer/UserInterface.connect("restartLevel", self, "restart")
	$PauseLayer/UserInterface.connect("diffChange", self, "diffChange")
	$PauseLayer/UserInterface.connect("changeEffectSound", self, "changeEffectSound")
	restart()


func changeEffectSound(value):
	$Level/Rocket/sound.volume_db = value
	$Level/Rocket/explosion.volume_db = value


func diffChange(id: int):
	match id:
		0: # easy
			$Spawner.stop()
		1: # normal
			$Spawner.start()
			$Spawner.wait_time = 2
		2: # hard
			$Spawner.start()
			$Spawner.wait_time = 1


func restart():
	$Level/Platform.position = Vector2(rand_range(80, get_viewport().size.x - 100), get_viewport().size.y)
	$Level/Rocket.needsReset = true
	var asteroids = get_tree().get_nodes_in_group("asteroids")
	for i in asteroids:
		i.queue_free()


func rotChange(value):
	$Level/Rocket.roll_speed = value


func win():
	pause_overlay.get_node("winsound").play()
	pause_overlay.get_node("pauseText").text = "Yeah you made it"
	togglePause()


func _on_Area2D2_body_entered(body: Node) -> void:
	if body is Rocket:
		$Level/Rocket/expeff.set_emitting(1)
		$Level/Rocket.get_node("explosion").play()
		pause_overlay.get_node("pauseText").text = "Game Over"
		togglePause()


func _on_Spawner_timeout() -> void:
	var spawn_location = $Path2D/AsteroidSpanLocation
	spawn_location.unit_offset = randf()
	var asteroid = asteroid_scene.instance()
	add_child(asteroid)
	asteroid.add_to_group("asteroids")

	asteroid.position = spawn_location.position
	var dir = spawn_location.rotation + PI / 2
	dir += rand_range(-PI / 4, PI / 4)
	asteroid.rotation = dir

	var velocity = Vector2(rand_range(asteroid.min_speed, asteroid.max_speed), 0)
	asteroid.linear_velocity = velocity.rotated(dir)
