extends Control

onready var scene_tree: = get_tree()
onready var pause_overlay:ColorRect = $PauseOverlay
onready var pause_text:Label = $PauseOverlay/pauseText
onready var slider:HSlider = $PauseOverlay/bgsoundslider
onready var rotSlider:HSlider = $PauseOverlay/rotationslider
onready var sound: AudioStreamPlayer =$PauseOverlay/backgroundmusic
onready var soundEff: AudioStreamPlayer =$PauseOverlay/winsound
onready var soundEffSlider:HSlider = $PauseOverlay/effectSlider
onready var popup: PopupMenu = $PauseOverlay/MenuButton.get_popup()


export (PackedScene) var TitleScreen

signal rotationChange(value)
signal restartLevel(value)
signal diffChange(value)
signal changeEffectSound


func _ready() -> void:
	popup.connect("id_pressed", self, "id_pressed")


func id_pressed(id: int):
	for i in popup.items.size():
		popup.set_item_checked(i, false)
	popup.set_item_checked(id, true)
	match id:
		0: # easy
			rotSlider.value = 5
		1: # normal
			rotSlider.value = 15
		2: # hard
			rotSlider.value = 22
	emit_signal("diffChange",id)


func togglePause():
	var isPaused = !scene_tree.paused
	scene_tree.paused = isPaused
	pause_overlay.visible  = isPaused


func _input(event):
	if event.is_action_released("pause") && pause_text.text == "Pause":
		togglePause()


func _on_MainMenuBtn_pressed() -> void:
	togglePause()
	scene_tree.change_scene(TitleScreen.get_path())


func _on_RestartBtn_pressed() -> void:
	togglePause()
	pause_text.text = "Pause"
	emit_signal("restartLevel")


func _on_bgsoundslider_value_changed(value: float) -> void:
	sound.set_volume_db(slider.get_value())


func _on_Button_toggled(button_pressed: bool) -> void:
	if sound.is_playing():
		sound.stop()
	else:
		sound.play()


func _on_rotationslider_value_changed(value: float) -> void:
	emit_signal("rotationChange", rotSlider.get_value())


func _on_effect_value_changed(value: float) -> void:
	soundEff.set_volume_db(soundEffSlider.get_value())
	emit_signal("changeEffectSound",soundEffSlider.get_value())
