class_name Rocket
extends RigidBody2D

export var roll_speed = 5
export var boost_coef = 10
export var needsReset = false

onready var thrusters: Particles2D = $thrusterParticles
onready var sound: AudioStreamPlayer2D = $sound


func _input(event):
	if event.is_action_pressed("move_up"):
		if !thrusters.is_emitting():
			thrusters.set_emitting(1)
		if !sound.is_playing():
			sound.play()
	if event.is_action_released("move_up"):
		thrusters.set_emitting(0)
		sound.stop()


func _integrate_forces(state :Physics2DDirectBodyState):
	if needsReset:
		needsReset = false
		sound.stop()
		thrusters.set_emitting(0)
		state.set_angular_velocity(0)
		state.set_linear_velocity(Vector2(0,0))
		state.transform = Transform2D(0, Vector2(rand_range(80 ,get_viewport().size.x * 0.9), get_viewport().size.y * 0.1))
	if (Input.is_action_pressed("move_up")):
		apply_central_impulse(Vector2(boost_coef * sin(rotation), -boost_coef * cos(rotation)))
	if Input.is_action_pressed("move_right"):
		apply_torque_impulse(roll_speed)
	if Input.is_action_pressed("move_left"):
		apply_torque_impulse(-roll_speed)
