extends RigidBody2D

export var min_speed = 250.0
export var max_speed = 350.0

func _ready() -> void:
	var types = $AnimatedSprite.frames.get_animation_names()
	$AnimatedSprite.animation = types[randi() % types.size()]


func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()
