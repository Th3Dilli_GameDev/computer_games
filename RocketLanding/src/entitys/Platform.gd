extends StaticBody2D

signal win

func _on_Area2D_body_entered(body: Node) -> void:
	if body is Rocket:
		$Timer.start()


func _on_Timer_timeout() -> void:
	var bodies = $Area2D.get_overlapping_bodies()
	for body in bodies:
		if body is Rocket:
			emit_signal("win")

