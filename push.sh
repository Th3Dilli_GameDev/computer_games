butler push --if-changed builds/html th3dilli/RocketLanding:HTML
butler push --if-changed builds/win th3dilli/RocketLanding:win
butler push --if-changed --fix-permissions builds/mac th3dilli/RocketLanding:mac
butler push --if-changed --fix-permissions builds/linux th3dilli/RocketLanding:linux
